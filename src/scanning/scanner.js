import './scannable'

import {cellSize, start, cellY} from '../territory/ownership'
import {V} from 'playcanvas-working-vectors'

const Scanner = pc.createScript('scanner')
Scanner.attributes.set({
    ignore: pc.attr.string.default('ignore')
})

const letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J']

Scanner.prototype.initialize = function() {
    let candidates = this.entity.ofType('scannable')
    candidates.map(c=>c.entity).forEach(candidate=>{
        let position = candidate.getPosition()
        let {row, col} = getCellFromCoordinates(position.x, position.z)
        let colLetter = letters[col]
        if(row >= 0 && row <= 5 && colLetter) {
            candidate.broadcast('located', {row, col, colLetter})
        }
    })
}

const result = {row: 0, col: 0}

function getCellFromCoordinates(x, z) {
    if(x.x) {
        z = x.z
        x = x.x
    }
    let row = Math.round((z - start.y) / cellSize.y)
    let col = Math.round((x - start.x) / cellSize.x)
    let index = row * 10 + col
    return {row, col, index}
}

function getCoordinatesFromCell(col, row) {
    return V(start.x + cellSize.x * col, cellY, start.y + cellSize.y * row)
}

export {
    getCellFromCoordinates,
    getCoordinatesFromCell
}

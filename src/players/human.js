import {makeReinforcements} from '../reinforcements'
import {getCoordinatesFromCell} from "../scanning/scanner";
import getCells from 'game-cells'
import delay from 'playcanvas-delay'

const Human = pc.createStateMachine(pc.createScript('human'))
Human.attributes.set({
    numberOfReinforcements: pc.attr.number.default(10),
    numberAtStart: pc.attr.number.default(5)
})
Human.prototype.initialize = function () {
    this.reinforcements = []
    this.items = []
    this.app.on('newgame', this.makeReinforcements, this)
    this.app.on('lost', item=>{
        let idx = this.items.indexOf(item)
        if(idx != -1) {
            this.items.splice(idx,1)
            this.app.fire('troops', this.items.length)
        }
    })
}

Human.prototype.makeReinforcements = async function (number) {
    number = number || (this.numberOfReinforcements + this.numberAtStart)
    this.reinforcements = makeReinforcements(number, definition => {
        return !!pc.prefabs[`ally_${definition.type}`]
    })
    this.layoutReinforcements()
    this.app.fire('troops', this.items.length)
    do {
        this.spawnTransmission()
        await delay(3000 + 20000 * Math.random())
    } while(true)
}

Human.prototype.spawnTransmission = function() {
    let row = Math.floor(Math.random() * 3) + 3
    let col = Math.floor(Math.random() * 10)
    let transmission = pc.prefabs.transmission()
    transmission.enabled = true
    let pos = getCoordinatesFromCell(col, row).addX(Math.random()/3).addY(-Math.random()/3).Y(0.06)
    transmission.setPosition(pos)
}

function extractItem(array) {
    let index = Math.floor(Math.random() * array.length)
    let result = array[index]
    array.splice(index, 1)
    return result
}

Human.prototype.layoutReinforcements = function () {
    this.items.forEach(item => item.return())
    this.items = []
    let cells = getCells(info => gameMap.owner[info.index].ally).slice(0)
    for (let i = 0; i < this.numberAtStart; i++) {
        let troop = this.reinforcements[i]
        let cell
        let count = 0
        do {
            cell = extractItem(cells)
            if(troop.water && gameMap.geography[cell].water) {
                break
            }
            if(troop.land && !gameMap.geography[cell].water) {
                break
            }
            cells.push(cell)
        } while (count++ < 60)
        if (count >= 60) continue
        let row = Math.floor(cell / 10)
        let col = cell % 10
        let instance = pc.prefabs[`ally_${troop.type}`]()
        this.items.push(instance)
        instance.setMapPosition(getCoordinatesFromCell(col, row))

    }
    this.reinforcements.splice(0, this.numberAtStart)
    this.reinforcements.forEach((troop, index) => {
        let instance = pc.prefabs[`ally_${troop.type}`]()
        this.items.push(instance)
        instance.setPosition(getCoordinatesFromCell(index, 7))
        instance.enabled = true
    })
}


import {makeReinforcements} from '../reinforcements'
import {getCoordinatesFromCell} from "../scanning/scanner";
import getCells from 'game-cells'

const Enemy = pc.createStateMachine(pc.createScript('enemy'))
Enemy.attributes.set({
    numberOfReinforcements: pc.attr.number.default(10),
    numberAtStart: pc.attr.number.default(5)
})
Enemy.prototype.initialize = function () {
    this.reinforcements = []
    this.items = []
    this.app.on('newgame', this.makeReinforcements, this)
}

Enemy.prototype.makeReinforcements = function (number) {
    number = number || (this.numberOfReinforcements + this.numberAtStart)
    this.reinforcements = makeReinforcements(number, definition => {
        return !!pc.prefabs[`enemy_${definition.type}`]
    })
    this.layoutReinforcements()
}

function extractItem(array) {
    let index = Math.floor(Math.random() * array.length)
    let result = array[index]
    array.splice(index, 1)
    return result
}

Enemy.prototype.layoutReinforcements = function () {
    this.items.forEach(item => item.return())
    this.items = []
    let cells = getCells(info => gameMap.owner[info.index].enemy).slice(0)
    for (let i = 0; i < this.numberAtStart; i++) {
        let troop = this.reinforcements[i]
        let cell
        let count = 0
        do {
            cell = extractItem(cells)
            if (troop.water && gameMap.geography[cell].water) {
                break
            }
            if (troop.land && !gameMap.geography[cell].water) {
                break
            }
            cells.push(cell)
        } while (count++ < 60)
        if(count >= 60) continue
        let row = Math.floor(cell / 10)
        let col = cell % 10
        let instance = pc.prefabs[`enemy_${troop.type}`]()
        this.items.push(instance)
        instance.setMapPosition(getCoordinatesFromCell(col, row))


    }
    this.reinforcements.splice(0, this.numberAtStart)
    this.reinforcements.forEach((troop, index) => {
        let instance = pc.prefabs[`enemy_${troop.type}`]()
        this.items.push(instance)
        instance.setPosition(getCoordinatesFromCell(index, 7))
        instance.enabled = true

    })

    this.items.forEach(item=>item.broadcast('hide', true))
}


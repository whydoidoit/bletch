import asset from 'playcanvas-awaitassets'
import delay from 'playcanvas-delay'
import {V} from 'playcanvas-working-vectors'
import {getCellFromCoordinates} from "./scanning/scanner";
import update from 'update'

const Attack = pc.createScript('attack')
Attack.prototype.initialize = async function() {
    this.threatMatrix = (await asset(10883431)).resource
    this.camera = this.app.root.findByName('Camera')
}

function convert(type) {
    return type.slice(type.indexOf('_')+1)
}

const offset = new pc.Vec3(0,1,1.2)

Attack.prototype.attack = async function(other) {
    let won = ()=> {
        if (!gameMap) return
        let owner = gameMap.owner[getCellFromCoordinates(this.entity.getPosition()).index]
        if (!owner) return
        if (this.entity.tags.has('enemy')) {
            owner.ally = false
            owner.enemy = true
        } else {
            owner.ally = true
            owner.enemy = false
        }
        this.app.fire('refreshterritory')
        return
    }
    let lost = () => {
        if (!gameMap) return
        let owner = gameMap.owner[getCellFromCoordinates(this.entity.getPosition()).index]
        if (!owner) return
        owner.ally = false
        owner.enemy = false
        this.app.fire('refreshterritory')
        return
    }
    if(!other) {
        return won()
    }
    let myType = convert(this.entity.ofType('prefab')[0].type)
    let number = this.threatMatrix.types.length
    let myIndex = this.threatMatrix.types.indexOf(myType)
    if(myIndex < 0) return

    let otherType = convert(other.entity.ofType('prefab')[0].type)
    let otherIndex = this.threatMatrix.types.indexOf(otherType)
    if(otherType < 0) return

    let outcome = this.threatMatrix.threats[number * myIndex + otherIndex]
    await delay(100)
    this.app.fire('focus', V(this.entity.getPosition()).add(offset))
    if(outcome < 0) {
        await spin(this.entity)
        this.entity.broadcast('hide')
        await delay(1000)
        this.entity.return()
    } else if(outcome >= 1) {
        await spin(other.entity)
        other.entity.broadcast('hide')
        won()
        await delay(1000)
        other.entity.return()

    } else if (outcome > 0) {
        spin(other.entity)
        await spin(this.entity)
        other.entity.broadcast('hide')
        this.entity.broadcast('hide')
        await delay(1000)
        lost()

    } else {
        await delay(1000)
    }
    this.app.fire('resetfocus')
}

function spin(item) {
    return new Promise(resolve=>{
        let speed = 50
        let angles = item.getLocalEulerAngles()
        update(dt=>{
            speed += dt * 100
            angles.y += speed * dt
            item.setLocalEulerAngles(angles)
            if(speed >= 600) resolve()
            return speed < 1000
        })
    })
}

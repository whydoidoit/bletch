
const Camera = pc.createScript('camera')

const states = {}

Camera.prototype.initialize = function() {
    this.initialPosition = new pc.Vec3().copy(this.entity.getPosition())
    this.targetPosition = new pc.Vec3().copy(this.initialPosition)
    this.app.on('focus', position => {
        this.targetPosition.copy(position)
    })
    this.app.on('resetfocus', ()=>{
        this.targetPosition.copy(this.initialPosition)
    })
    window.gameCamera = this
}

Camera.prototype.update = function(dt) {
    let pos = this.entity.getPosition()
    pos.lerp(pos, this.targetPosition, dt * 3)
    this.entity.setPosition(pos)
}




import types from 'piece-types'

const availableTroops = types.types.map(type => Object.assign({type}, types.definitions[type])).filter(t => !!t.allocation)
const maxWeight = availableTroops.reduce((c, troop) => c + troop.allocation, 0)

function returnTrue() {
    return true
}

function allocate(predicate = returnTrue) {
    let value = maxWeight * Math.random()
    for (var i = 0; true; i = (i + 1) % availableTroops.length) {
        value -= availableTroops[i].allocation || 1
        if (value <= 0 && predicate(availableTroops[i])) {
            return availableTroops[i]
        }
    }
}

function makeReinforcements(number, predicate) {
    const array = []
    for (let i = 0; i < number; i++) {
        array.push(allocate(predicate))
    }
    return array
}

export {
    allocate, makeReinforcements
}

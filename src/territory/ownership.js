import asset from 'playcanvas-awaitassets'

const cellSize = {x: 0.493, y: 0.51 }
const cellY = 0.04
const start = {x: 0.253, y: 0.274}

const Territory = pc.createScript('territory')
Territory.attributes.set({
    ally: pc.attr.entity,
    enemy: pc.attr.entity
})

Territory.prototype.initialize = async function() {

    let target = this.allies = []
    let item
    for (let y = 0; y < 6; y++) {
        for(let x = 0; x < 10; x++) {
            target.push(item = this.ally.clone())
            this.app.root.addChild(item)
            item.enabled = false
            item.setPosition(start.x + x * cellSize.x, cellY, start.y + y * cellSize.y)
        }
    }
    target = this.enemies = []
    for (let y = 0; y < 6; y++) {
        for (let x = 0; x < 10; x++) {
            target.push(item = this.enemy.clone())
            this.app.root.addChild(item)
            item.enabled = false
            item.setPosition(start.x + x * cellSize.x, cellY, start.y + y * cellSize.y)
        }
    }
    this.ally.enabled = false
    this.enemy.enabled = false
    window.gameMap = this.map = (await asset(10881302)).resource
    gameMap.troops = []
    for(let i = 0; i < 60; i++) {
        gameMap.troops.push([])
    }
    this.refresh()

    this.app.on('moved', this.piecesMoved, this)
    this.app.on('refreshterritory', this.refresh, this)
}

Territory.prototype.refresh = function() {
    let enemies = this.enemies
    let allies = this.allies
    let map = this.map.owner
    let allycount = 0
    for (let y = 0, index = 0; y < 6; y++) {
        for (let x = 0; x < 10; x++, index++) {
            enemies[index].enabled = map[index].enemy
            allies[index].enabled = map[index].ally
            if(map[index].ally) {
                allycount++
            }
        }
    }
    this.app.fire('refreshedterritory', allycount)
}

export  {
    cellSize, cellY, start
}

import delay from 'playcanvas-delay'
import getCells from 'game-cells'
import {V} from 'playcanvas-working-vectors'

const Transmission = pc.createScript('transmission')

Transmission.prototype.removedFromPool = async function() {
    await delay(2000 + Math.random() * 15000)
    this.entity.return()
}

const offset = new pc.Vec3(0, 2, 1)

Transmission.prototype.tapped = async function() {
    let cells = getCells(info => gameMap.owner[info.index].enemy && gameMap.troops[info.index].length==1 && !gameMap.troops[info.index].visible).slice(0)
    if(!cells.length) return
    let troop = gameMap.troops[cells[Math.floor(Math.random() * cells.length)]][0]
    if(!troop || troop.visible) return
    this.entity.return()
    this.app.fire('focus', V(troop.entity.getPosition()).add(offset))
    await delay(400)
    troop.show()
    await delay(2000)
    this.app.fire('resetfocus')

}

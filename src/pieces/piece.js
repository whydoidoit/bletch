import {getCellFromCoordinates, getCoordinatesFromCell} from "../scanning/scanner"
import {V} from 'playcanvas-working-vectors'
import Easing from 'easing'
import {cellSize} from "../territory/ownership";

const Piece = pc.createStateMachine(pc.createScript('piece'))


const states = {}

let active = null

Piece.prototype.initialize = function() {
    this.entity.piece = this
    this.visible = true
    this.spot = this.entity.findByName('Spot Light')
    this.register('update tapped show hide', true)
    this.define(states)
    this.created = Date.now()
    this.initialPosition = new pc.Vec3(this.entity.getPosition())
    this.targetPosition = new pc.Vec3
    this.startPosition = new pc.Vec3
    this.difference = new pc.Vec3
    this.state = 'waiting'
    const collider = this.collider = this.entity.ofType('collision')[0].entity
    this.raycastOptions = {
        predicate(entity) {
            return entity != collider
        }
    }
    const working = new pc.Vec3
    this.entity.setMapPosition = function(pos, y, z) {
        if(this.lastPos) {
            let cell = gameMap.troops[this.lastPos.col + this.lastPos.row * 10]
            if(cell) {
                let idx = cell.indexOf(this)
                if(idx != -1) {
                    cell.splice(idx, 1)
                }
            }
        }
        if(y) {
            working.x = pos
            working.y = y
            working.z = z
        } else {
            working.copy(pos)
        }
        let tpos = getCellFromCoordinates(working.x, working.z)
        let index = tpos.col + tpos.row * 10
        let cell = gameMap.troops[index];
        if(cell) {
            cell.forEach(item=>{
                item.entity.broadcast('entered', this.entity)
            })
            this.entity.broadcast('attack', cell[0])
            cell.push(this)
            this.lastPos = tpos
        } else {
            this.lastPos = null
        }
        return this.entity.setPosition(working)
    }.bind(this)
}

const OFFSCREEN = new pc.Vec3(-200000,-200000,-20000)

Piece.prototype.addedToPool = function() {
    this.entity.setMapPosition(OFFSCREEN)
    this.app.fire('lost', this.entity)
}

Piece.prototype.entered = function() {
    if(!this.visible) {
        this.show()
        setTimeout(()=>{
            this.hide()
        }, 10000 + Math.random() * 25000)
    }
}

Piece.prototype.move = function(hit) {
    if(hit.entity.name == 'Board Collision') {
        let {row, col} = getCellFromCoordinates(hit.point.x, hit.point.z)
        if(row < 0 || row > 5 || col < 0 || col > 9) return
        let data = {
            row, col, allow: true, cell: gameMap.troops[row * 10 + col]
        }
        let similar = data.cell.filter(item=>this.entity.tags.has('enemy') ? item.entity.tags.has('enemy') : item.entity.tags.has('ally'))
        if(similar.length) return
        this.app.fire('select', data)
        this.entity.broadcast('selectDestination', data)
        if(!data.allow) return
        this.app.fire('move', data)
        this.moveData = data
        let pos = getCoordinatesFromCell(col, row)
        let target
        if(data.cell.length) {
            let offset = this.entity.tags.has('enemy') ? 1 : -1
            target = V(pos).addX(offset * cellSize.x/4).addZ(offset * cellSize.y/4)
            data.cell.forEach(item=>{
                let epos = item.entity.getPosition()
                let y = epos.y
                let rc = getCellFromCoordinates(epos.x, epos.z)
                let pos = getCoordinatesFromCell(rc.col, rc.row)
                pos.addX(-offset * cellSize.x/4).addZ(-offset * cellSize.y/4).Y(y)
                item.entity.setPosition(pos)
            })
        } else {
            target = V(pos)
        }
        let y = pos.y
        let result = pc.CollisionDetection.raycast(V(this.entity.getPosition()).addY(0.1), V(target).addY(0.1), this.raycastOptions)
        this.targetPosition.copy(target)
        this.state = result.length ? 'jump' : 'direct'
    }
}

const Waiting = states.waiting = {}

Waiting.tapped = function() {
    if(active) {
        active.state = 'waiting'
    }
    active = this
    this.state = 'highlight'
}

Waiting.show = function(immediate) {
    if(this.visible) return
    this.visible = true
    this.immediate = immediate
    this.state = 'visible'
}

Waiting.hide = function(immediate) {
    if (!this.visible) return
    this.immediate = immediate
    this.visible = false
    this.state = 'hidden'
}

const Hidden = states.hidden = {}

Hidden.enter = function() {
    if((Date.now() - this.created < 1000) || this.immediate) {
        this.t = 1
    } else {
        this.t = 0
    }
    this.direction = 1
    this.state = 'rising'
    this.visible = false
}

const Rising = states.rising = {}

Rising.enter = function() {
    this.immediate = false
}

Rising.update = function(dt) {
    this.t += dt * this.direction
    let t = pc.math.clamp(this.t)
    let position = this.entity.getPosition()
    position.y = Easing.sineInOut(t) * -0.8
    this.entity.setPosition(position)
    if(t < 0 || t>1) this.state = 'waiting'
}

const Visible = states.visible = {}

Visible.enter = function() {
    if ((Date.now() - this.created < 1000) || this.immediate) {
        this.t = 0
    } else {
        this.t = 1
    }
    this.direction = -1
    this.state = 'rising'
    this.visible = true
}



const Highlight = states.highlight = {}

Highlight.enter = function() {
    this.spot.light.enabled = true
    this.app.on('tapped', this.move, this)
}

Highlight.tapped = function() {
    if(active == this) {
        active = null
        this.state = 'waiting'
    }
}

Highlight.exit = function() {
    this.spot.light.enabled = false
    this.app.off('tapped', this.move, this)
}

const Jump = states.jump = {}

Jump.enter = function() {
    this.t = 0
    this.currentRotation  = this.entity.getLocalEulerAngles().y
    this.targetRotation = Math.random() * 50 + -22
    this.startPosition.copy(this.entity.getPosition())
    this.y = this.startPosition.y
    this.difference.copy(this.targetPosition).sub(this.startPosition)
}

Jump.update = function(dt) {
    this.t += dt
    let t = pc.math.clamp(Easing.quadInOut(this.t))
    this.entity.setPosition(V(this.startPosition).add(V(this.difference).scale(t)).Y(this.y + 0.8 * Math.sin(Math.PI * pc.math.clamp(this.t))))
    let angles = this.entity.getLocalEulerAngles()
    angles.y = pc.math.lerp(this.currentRotation, this.targetRotation, pc.math.clamp(this.t))
    this.entity.setLocalEulerAngles(angles)
    if(this.t>=1) this.state = 'waiting'
}

Jump.exit = function() {
    this.entity.setMapPosition(V(this.entity.getPosition().Y(this.y)))
    this.app.fire('moved', this.moveData)
}

const Direct = states.direct = {}

Direct.enter = Jump.enter

Direct.update = function (dt) {
    this.t += dt
    let t = pc.math.clamp(Easing.quadInOut(this.t))
    this.entity.setPosition(V(this.startPosition).add(V(this.difference).scale(t)))
    let angles = this.entity.getLocalEulerAngles()
    angles.y = pc.math.lerp(this.currentRotation, this.targetRotation, pc.math.clamp(this.t))
    this.entity.setLocalEulerAngles(angles)
    if (this.t >= 1) this.state = 'waiting'
}

Direct.exit = function() {
    this.entity.setMapPosition(this.entity.getPosition())
    this.app.fire('moved', this.moveData)
}


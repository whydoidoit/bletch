import {V} from 'playcanvas-working-vectors'

const raycastOptions = {max: 1, predicate(entity) {
    return (!entity.parent || !entity.parent.tags.has('enemy')) && !entity.tags.has('enemy')
    }}
const Controls = pc.createScript('controls')
Controls.attributes.set({
    camera: pc.attr.entity
})
Controls.prototype.initialize = function () {
    this.castThrough = new pc.Vec3
    this.app.mouse.disableContextMenu()
    this.app.mouse.on(pc.EVENT_MOUSEDOWN, this.onMouseDown, this)
}

Controls.prototype.onMouseDown = function(ev) {
    let camera = this.camera.camera
    let pos = camera.screenToWorld(ev.x, ev.y, 10, V())
    let position = this.camera.getPosition();
    pos.sub(position).normalize()
    let cast = pc.CollisionDetection.raycastDirection(position, pos, 20, raycastOptions)
    if(cast.length > 0) {
        cast[0].entity.sendUpwards('tapped')
        pc.app.fire('tapped', cast[0])
    }
}

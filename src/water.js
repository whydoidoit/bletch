const Water  = pc.createScript('water')

Water.prototype.selectDestination = function(coords) {
    if(!gameMap.geography[coords.row * 10 + coords.col].water) {
        coords.allow = false
    }
}

const Land = pc.createScript('land')

Land.prototype.selectDestination = function(coords) {
    if (gameMap.geography[coords.row * 10 + coords.col].water) {
        coords.allow = false
    }
}
